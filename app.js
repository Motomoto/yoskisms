const http = require('http');
const express = require('express');
const MessagingResponse = require('twilio').twiml.MessagingResponse;

const app = express();

app.post('/gifts', (req, res) => {
  console.log('Received hook:', req)
  const twiml = new MessagingResponse();
  twiml.message('Your free gift is waiting for you here -> https://bit.ly/2Rp9weU');

  res.writeHead(200, {'Content-Type': 'text/xml'});
  res.end(twiml.toString());
  console.log('Sending response:', twiml.toString())
});

app.get('/', (req, res) => {
  res.send('Success');
});

http.createServer(app).listen(3000, '0.0.0.0' () => {
  console.log('Express server listening on port 3000');
});